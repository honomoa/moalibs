import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.Iterator;

import org.apache.log4j.Logger;

/**
 * $Revision$
 * $Date$
 * $Author$
 * 
 * $Id$
 */

public class MoaLibs {

	public static Logger logger;
	public static void main(String[] argc){
		listSystemProperties();
	}
	public static void listSystemProperties(){
		Iterator it = System.getProperties().keySet().iterator();
		while(it.hasNext()){
			String env = (String)it.next();
			System.out.println(env+"="+System.getProperty(env)+"<br/>");
		}
	}
	
	public static void printIteratorable(Iterator it){
		while(it.hasNext()){
			String env = (String)it.next();
			System.out.println(env);
		}
	}

	public static void methodRunningTime(long repeat, Class clazz, Object[] classParams, String methodName, Object[] methodPparams) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SecurityException, NoSuchMethodException, IllegalArgumentException, InvocationTargetException{
		Constructor constructor;
		int i;
		
		if(classParams==null){
			constructor = clazz.getConstructor();
		}
		else{
			Class[] classParamClass = new Class[classParams.length];
			i=0;
	        for(Object o:classParams){
	        	if(o instanceof String)
	        		classParamClass[i++] = String.class;
	        	else if(o instanceof Integer)
	        		classParamClass[i++] = Integer.class;
	        	else if(o instanceof Long)
	        		classParamClass[i++] = Long.class;
	        	else if(o instanceof Double)
	        		classParamClass[i++] = Double.class;
	        	else if(o instanceof Float)
	        		classParamClass[i++] = Float.class;
	        	else if(o instanceof File)
	        		classParamClass[i++] = File.class;
	        	else if(o == null)
	        		classParamClass[i++] = null;
	        }
    		constructor = clazz.getConstructor(classParamClass);
		}
        Object targetObj = constructor.newInstance(classParams);
        
        Class[] methodParamClass = new Class[methodPparams.length];
        i=0;
        for(Object o:methodPparams){
        	if(o instanceof String)
        		methodParamClass[i++] = String.class;
        	else if(o instanceof Integer)
        		methodParamClass[i++] = Integer.class;
        	else if(o instanceof Long)
        		methodParamClass[i++] = Long.class;
        	else if(o instanceof Double)
        		methodParamClass[i++] = Double.class;
        	else if(o instanceof Float)
        		methodParamClass[i++] = Float.class;
        	else if(o instanceof File)
        		methodParamClass[i++] = File.class;
        }
        Method method = clazz.getMethod(methodName, methodParamClass);
        
        Object[] paramObjs1 = methodPparams;
        
		long start, end, counter, total;
		counter = repeat;
		total = 0;
		while (counter-- > 0) {
			start = System.nanoTime();
			method.invoke(targetObj, paramObjs1);
			end = System.nanoTime();
			total += (end - start);
		}
		System.out.println();
		System.out.println("average: "+total/repeat+" nano seconds");
	}
	
	public static void printAllCharsetFormat(String str){
		for (String encoding : Charset.availableCharsets().keySet()) {
            try {
                byte[] binary = str.getBytes(encoding);
                for (String encoding2 : Charset.availableCharsets().keySet()) {
                	if(!encoding.equals(encoding2)){
                		String s = String.format("%s to %s: %s%n", encoding,encoding2, new String(binary, encoding2));
//                		System.out.println(s);
                		logger.info(s);
                	}
                }
            } catch (Exception e) {
            }
        }
	}
	public static void printAllCharsetFormat(byte[] str){
		printAllCharsetFormat(new String(str));
	}
	public static void printAllCharsetFormat(char[] str){
		printAllCharsetFormat(new String(str));
	}
}
